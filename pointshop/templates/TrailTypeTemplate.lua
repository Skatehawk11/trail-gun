ITEM.Name = 'TestTrail'
ITEM.Price = 10
ITEM.Material = 'trails/test.vmt' -- Replace the ".vmt with the trail you want"

function ITEM:OnEquip(ply, modifications)
	 ply:SetNWString( "Trail", "trails/test.vmt" ) -- Replace the ".vmt with the trail you want"
end

function ITEM:OnHolster(ply)
	ply:SetNWString( "Trail", "trails/test.vmt" ) -- Replace the ".vmt with the trail you want to have after it is unequipped"
end
