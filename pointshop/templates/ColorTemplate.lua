ITEM.Name = 'Color'
ITEM.Price = 10
ITEM.Model = 'models/props_lab/binderredlabel.mdl' -- Model to represent the color (Currently binder colors)
 
function ITEM:OnEquip(ply, modifications)

	ply:SetNWInt( "red", 255) -- Replace number with RGB value
	ply:SetNWInt( "green", 0) -- Replace number with RGB value
	ply:SetNWInt( "blue", 0) -- Replace number with RGB value
end

function ITEM:OnHolster(ply)
	ply:SetNWInt( "red", 255) -- Replace number with RGB value after it is un-equiped 
	ply:SetNWInt( "green", 255) -- Replace number with RGB value after it is un-equiped 
	ply:SetNWInt( "blue", 255) -- Replace number with RGB value after it is un-equiped 
end
