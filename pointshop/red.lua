ITEM.Name = 'Red'
ITEM.Price = 10
ITEM.Model = 'models/props_lab/binderredlabel.mdl'
 
function ITEM:OnEquip(ply, modifications)

	ply:SetNWInt( "red", 255)
	ply:SetNWInt( "green", 0)
	ply:SetNWInt( "blue", 0)
end

function ITEM:OnHolster(ply)
	ply:SetNWInt( "red", 255)
	ply:SetNWInt( "green", 255)
	ply:SetNWInt( "blue", 255)
end
