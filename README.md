# README #

To use the trailgun drag and drop the "ttt_trailgun" folder into the servers addons folder and drag the pointshop folder into the pointshop/lua/items/trailgun/ folder (create the trailgun folder.). 

To give players the trailgun create a script in the autorun folder of the addon and use the ply:give() function to give the weapon.

Go to autorun/config.lua to configure the trailgun

Contact me on steam if you have any questions or problems.