
if (SERVER) then
	AddCSLuaFile ()
	
else


SWEP.PrintName = "Trail Gun"
SWEP.Author = "Skatehawk11"
SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.Primary.Ammo = "none"
SWEP.Primary.Delay = 5
SWEP.Primary.ClipSize = -1
SWEP.Primary.ClipMax = -1
SWEP.Primary.DefaultClip = -1
SWEP.Primary.Automatic = false

SWEP.UseHands = false

SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 54
SWEP.ViewModel = "models/weapons/v_crowbar.mdl"
SWEP.WorldModel = "models/weapons/w_crowbar.mdl"

SWEP.AllowDrop = false
SWEP.IsSilent = false
SWEP.NoSights = true
SWEP.DrawCrosshair = true


end


function SWEP:Initialize()
self:SetWeaponHoldType( "normal" )
end

function SWEP:Deploy()
	if SERVER then
		self.Owner:DrawWorldModel(false)
		self.Owner:DrawViewModel(false)
	end
end


function SWEP:Reload()
end

function SWEP:PrimaryAttack()
	self.Weapon:SetNextPrimaryFire( CurTime() + TG.ShootDelay )
	self:ShootTrailGun()
end

function SWEP:SecondaryAttack()
	
end


function SWEP:ShootTrailGun()
	self:EmitSound(TG.ShootSound)
	if (CLIENT) then return end
	if (SERVER) then 
	
	local ent = ents.Create("prop_physics")
	local RED = self.Owner:GetNWInt("red", 255)
	local BLUE = self.Owner:GetNWInt("blue", 255)
	local GREEN = self.Owner:GetNWInt("green", 255)
	local trailcolor = Color(RED,GREEN,BLUE)
	local trailType = self.Owner:GetNWString( "Trail", TG.DefaultTrail )
	local trail = util.SpriteTrail(ent, 0, trailcolor, false, TG.StartWidth, TG.EndWidth, 4, 1/(15+1)*0.5, trailType)
	
	if (  !IsValid( ent ) ) then return end
	ent:SetModel( self.Owner:GetNWString( "TrailModel", "models/props_junk/PopCan01a.mdl" ) )
	ent:SetPos( self.Owner:EyePos() + ( self.Owner:GetAimVector() * 10 ) )
	ent:SetAngles( self.Owner:EyeAngles() )
	ent:Spawn()
 
	local phys = ent:GetPhysicsObject()
	if (  !IsValid( phys ) ) then ent:Remove() return end
 
	local velocity = self.Owner:GetAimVector()
	velocity = velocity * TG.Velocity
	velocity = velocity + ( VectorRand() * 10 ) -- a random element
	phys:ApplyForceCenter( velocity )
 
	timer.Simple( TG.RemoveTime, function() ent:Remove() end)

end

end
