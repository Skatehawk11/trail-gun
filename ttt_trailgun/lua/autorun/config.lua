TG = {}

--[[ Config ]] -- 


TG.Velocity = 1500 -- How fast should the projectile come out

TG.ShootSound = "Metal.SawbladeStick" -- Sound when fired

TG.ShootDelay = 5 -- How many seconds until you can fire again

TG.RemoveTime = 10 -- How many seconds until the the prop is removed

TG.StartWidth = 10 -- Start width of the trail

TG.EndWidth = 1 -- End width of the trail

TG.DefaultTrail = "trails/laser.vmt" -- Default Trail

